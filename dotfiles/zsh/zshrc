if [[ ! -n "$TMUX" && ! -f ~/.tmux.disable ]]; then
    source ~/.zshrc_d/tmux.source.zsh
    tmux::new
fi

export HISTSIZE=999999
export SAVEHIST=999999
export HISTFILE=~/.zsh_history
export UNAME=$(uname)
export UNAME_R=$(uname -r)

addpath_recursive () {
    readonly bin=$1

    if [ -e $bin ]; then
        find $bin -maxdepth 2 -type d | while read dir; do
            grep -q '\.' <<< "$dir" || export PATH=$PATH:$dir
        done
    fi
}

addpath_if () {
    readonly addif=$1

    if [ -d $addif ]; then
        export PATH=$PATH:$addif
    fi
}

addpath_if_front () {
    readonly addif=$1

    if [ -d $addif ]; then
        export PATH=$addif:$PATH
    fi
}

addpath_if $GOPATH/bin
addpath_if $HOME/bin
addpath_if $HOME/go/bin
addpath_if $HOME/scripts
addpath_if $HOME/.local/bin
addpath_if $HOME/.npm/node_modules/.bin
addpath_if $HOME/.cargo/bin
addpath_if $HOME/pkg/bin
addpath_if /usr/games
addpath_if /usr/local/go/bin
addpath_if /usr/local/scripts
addpath_recursive /opt/bin/
addpath_recursive /opt/snonux/bin/
addpath_recursive /opt/snonux/local/bin/
addpath_if_front /opt/local/bin/
addpath_if .

export PATH=$PATH:/sbin:/usr/sbin:/usr/local/sbin
export CDPATH=$CDPATH:$HOME

for dir in \
    ~/svn ~/svn/modules \
    ~/git ~/git/java ~/git/foo.zone-content/gemtext \
    ~/src \
    ~/Notes ~/Data ~/data; do
    test -d $dir && export CDPATH=$CDPATH:$dir
done

setopt histignorealldups sharehistory
setopt nobeep # never ever beep
setopt autocd # running /etc actually does a cd /etc
setopt cdablevars # avoid the need for an explicit $ in cd
setopt listtypes # show types in completion
setopt extendedglob # weird & wacky pattern matching - yay zsh!
setopt correct # spelling correction
setopt histverify # when using ! cmds, confirm first
setopt printexitvalue # alert me if something's failed

bindkey -v
bindkey "^R" history-incremental-search-backward

autoload -Uz compinit
compinit

autoload edit-command-line; zle -N edit-command-line
bindkey -M vicmd v edit-command-line

zstyle ':completion:*' auto-description 'specify: %d'
zstyle ':completion:*' completer _expand _complete _correct _approximate
zstyle ':completion:*' format 'Completing %d'
zstyle ':completion:*' group-name ''
zstyle ':completion:*' menu select=2
zstyle ':completion:*:default' list-colors ${(s.:.)LS_COLORS}
zstyle ':completion:*' list-colors ''
zstyle ':completion:*' list-prompt %SAt %p: Hit TAB for more, or the character to insert%s
zstyle ':completion:*' matcher-list '' 'm:{a-z}={A-Z}' 'm:{a-zA-Z}={A-Za-z}' 'r:|[._-]=* r:|=* l:|=*'
zstyle ':completion:*' menu select=long
zstyle ':completion:*' select-prompt %SScrolling active: current selection at %p%s
zstyle ':completion:*' use-compctl false
zstyle ':completion:*' verbose true
zstyle ':completion:*:kill:*' menu yes select
zstyle ':completion:*:kill:*' force-list always
zstyle ':completion:*:processes' command 'ps x -o pid,pcpu,tt,args'

alias ll='ls -trl'
alias la='ls -fA'
alias l='ls -CF'
alias ..='cd ..'
alias ...='cd ../..'
alias ....='cd ../../..'
alias s=sudo
alias d=doas
alias tomorrow='date +%Y%m%d --date tomorrow'
alias ttomorrow='date +%Y%m%d --date "2 days"'
alias tttomorrow='date +%Y%m%d --date "3 days"'
alias today='date +%Y%m%d'
alias today2='date +%Y-%m-%d'
alias yesterday='date +%Y%m%d --date yesterday'
alias yyesterday='date +%Y%m%d --date "2 days ago"'
alias yyyesterday='date +%Y%m%d --date "3 days ago"'
alias rsync_nostrict='rsync -e "ssh -o StrictHostKeyChecking=no"'
alias g='grep -E -i'
alias not='grep -E -i -v'
alias ro='fgrep " ro," /proc/mounts'
alias pl='pgrep -lf'
alias pf='pgrep -f'
alias first='awk "{ print \$1 }"'
alias fst=first
alias second='awk "{ print \$2 }"'
alias snd=second
alias third='awk "{ print \$3 }"'
alias thrd=third
alias lst='awk "{ print \$(NF) }"'
alias llst='awk "{ print \$(NF-1) }"'
alias lllst='awk "{ print \$(NF-2) }"'
alias se='cut -d"|" -f2'

rcloader::prepare () {
    local -r all_rcs="$1"; shift
    
    touch "$all_rcs.tmp" && chmod 600 "$all_rcs.tmp"

    for rcdir in $HOME/.zshrc_d/ \
        $HOME/.zshrc_{local,private,private_local,work,work_local}_d/; do
        if [ ! -e "$rcdir" ]; then
            continue
        fi    

        find "$rcdir" -name \*.source.zsh -type f -empty delete
        find "$rcdir" -name \*.source.zsh -type f | sort |
        while read -r rcfile; do
            echo "Preparing $rcfile" >&2
            cat "$rcfile"
            echo
        done
        [ -f ~/.zshrc_local ] && cat ~/.zshrc_local

    done > "$all_rcs".tmp
    mv "$all_rcs".tmp "$all_rcs"
}

rcloader::load () {
    # Speeding up loading ZSH on the mac (Security scanners)
    local -r all_rcs=~/.zshrc_d_all.source
    
    if [ ! -f "$all_rcs" ]; then
        rcloader::prepare "$all_rcs"
    fi

    source "$all_rcs"
}
alias loadzsh='rm ~/.zshrc_d_all.source; source ~/.zshrc'

rcloader::load

test "$UNAME" = FreeBSD && zpool list

if [ -f ~/motd ]; then
  echo
  echo "Local message of the day:"
  cat ~/motd
fi

supersync::is_it_time_to_sync
